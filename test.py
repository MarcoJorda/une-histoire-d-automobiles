import csv


# Lecture via dict reader

rows = []
new_rows = []

with open('test.csv', 'r') as csvin:
	reader = csv.DictReader(csvin, delimiter='~')
	for row in reader:
		rows.append(row)

print(rows)

for row in rows:
	new_row = {
		'firstname': row.get('col1'),
		'lastname': row.get('col2'),
	}
	new_rows.append(new_row)

print(new_rows)


with open('out.csv', 'w') as csvout:
	writer = csv.DictWriter(csvout, fieldnames=['firstname', 'lastname'], delimiter=',')
	writer.writeheader()
	for row in new_rows:
		writer.writerow(row)
