import unittest
import csv
from tests.fonctions_tests import lecture_fichier, ecriture_fichier, remplacement

class TestRemplacementCaractere(unittest.TestCase):
    
    ecriture_fichier()

    def test_lecture(self):
        self.assertEqual("Python", lecture_fichier())
    
    def test_remplacement(self):
        remplacement()
        self.assertEqual("Zython", lecture_fichier())
    
    
    