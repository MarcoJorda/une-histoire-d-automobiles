import csv

def ecriture_fichier():
        with open('test_ecriture.csv','w') as fichier_csv : 
                writer = csv.writer(fichier_csv)
                writer.writerow(['Python'])

def remplacement(): 
        
        ligne_remplacee = lecture_fichier()
        ligne_remplacee = ligne_remplacee.replace("P", "Z")
        with open('test_ecriture.csv', 'w') as fichier:
                fichier.write(ligne_remplacee) 

def lecture_fichier():     
        contenu = ""
        with open('test_ecriture.csv') as fichier_csv : 
                reader = csv.reader(fichier_csv)
                for ligne in reader:
                        contenu = contenu.join(ligne)
        return contenu

